from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpFaculty(models.Model):
    _name = 'academic.degree'
    _rec_name = 'x_name'

    x_name = fields.Char(string='Học hàm học vị')
    x_code = fields.Char(string='Mã')
