# -*- coding: utf-8 -*-
{
    'name': "Management Faculty",

    'summary': """
        Management Faculty """,

    'description': """
        Management Faculty
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
    ],

    # always loaded
    'data': [
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/op_faculty_view.xml',
        'views/academic_degree_view.xml',
        'views/menus.xml',
    ],
    'application': True,
}