from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpTiming(models.Model):
    _name = 'op.timing'
    _rec_name = 'x_name'

    x_name = fields.Char(string='Tên')
    x_type_timing = fields.Selection([
        ('am', 'Ca sáng'),
        ('pm', 'Ca chiều'),
        ('both', 'Cả ngày')
    ], default='am', string='Ca')