from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpSessions(models.Model):
    _name = 'op.session'
    _rec_name = 'x_name'

    x_name = fields.Char(string='Tên', default=lambda self: _('New'), required=True)
    x_faculty_id = fields.Many2one('op.faculty', string='Giảng viên')
    x_timing_id = fields.Many2one('op.timing', string='Ca', required=True)
    x_subject_id = fields.Many2one('op.subject', string='Môn học', required=True)
    x_course_id = fields.Many2one('op.course', string='Khóa học')
    x_batch_id = fields.Many2one('op.batch', string='Lịch đào tạo', required=True)
    x_start_date = fields.Date(string='Ngày bắt đầu')
    x_end_date = fields.Date(string='Ngày kết thúc')
    x_date = fields.Date(string='Ngày dạy', required=True)
    x_note = fields.Char(string='Ghi chú')
    color = fields.Integer('Color Index')

    @api.model
    def create(self, vals):
        if vals.get('x_name', _('New')) == _('New'):
            timing = self.env['op.timing'].browse(vals.get('x_timing_id'))
            subject = self.env['op.subject'].browse(vals.get('x_subject_id'))
            x_date = datetime.strftime(datetime.strptime(str(vals.get('x_date')), "%Y-%m-%d"), "%d/%m/%Y")
            vals['x_name'] = timing.x_name + '-' + x_date + '-' + subject.x_name
        result = super(OpSessions, self).create(vals)
        return result
