from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class GenerateTimetableLine(models.TransientModel):
    _name = 'generate.timetable.line'

    x_timing_id = fields.Many2one('op.timing', string='Ca', required=True)
    x_faculty_id = fields.Many2one('op.faculty', string='Giảng viên')
    x_subject_id = fields.Many2one('op.subject', string='Môn học', required=True)
    x_date = fields.Date(string='Ngày dạy', required=True)
    x_generate_timetable_id = fields.Many2one('generate.timetable', string='Tạo thời khóa biểu')
