# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from dateutil.relativedelta import relativedelta


class GenerateTimetable(models.TransientModel):
    _name = 'generate.timetable'
    _description = 'Generate Timetable'

    x_course_id = fields.Many2one('op.course', string='Khóa học')
    x_batch_id = fields.Many2one('op.batch', string='Lịch đào tạo', required=True)
    x_start_date = fields.Date(string='Ngày bắt đầu')
    x_end_date = fields.Date(string='Ngày kết thúc')
    x_generate_timetable_line_ids = fields.One2many('generate.timetable.line', 'x_generate_timetable_id',
                                                    string='Tạo thời khóa biểu chi tiết')

    @api.onchange('x_batch_id')
    def _onchange_x_batch_id(self):
        for rec in self:
            rec.write({
                'x_course_id': rec.x_batch_id.x_course_id.id,
                'x_start_date': rec.x_batch_id.x_start_date,
                'x_end_date': rec.x_batch_id.x_end_date
            })
            # for record in self:
            #     record.x_start_date = record.x_batch_id.x_start_date
            #     record.x_end_date = record.x_batch_id.x_end_date
            #     record.x_course_id = record.x_batch_id.x_course_id.id
            #     list_vals_line = []
            #     for line in record.batch_id.x_subject_details_ids:
            #         week = line.x_grade_weightage + line.x_theoretical_students
            #         end_date_sche = record.start_date + relativedelta(weeks=week)
            #         start_date = record.start_date
            #         vals = []
            #         while start_date <= end_date_sche:
            #             if int(start_date.weekday()) == int(line.x_learn_detail):
            #                 vals.append(start_date)
            #             start_date += relativedelta(days=1)
            #         if vals:
            #
            #             for date in vals:
            #                 vals_line = {
            #                     'subject_id': line.x_subject_name.id,
            #                     'x_date': date,
            #                     'x_article': str(date.weekday()),
            #                     'classroom_id': line.x_class_room.id,
            #                     'faculty_id': line.x_inherit_faculty_id.id,
            #                     'timing_id': line.x_timing_id.id
            #                 }
            #                 list_vals_line.append((0, 0, vals_line))
            #     self.write({
            #         'time_table_lines': list_vals_line,
            #     })

    def process(self):
        for rec in self:
            for line in rec.x_generate_timetable_line_ids:
                session = {
                    'x_faculty_id': line.x_faculty_id.id,
                    'x_timing_id': line.x_timing_id.id,
                    'x_subject_id': line.x_subject_id.id,
                    'x_course_id': rec.x_course_id.id,
                    'x_batch_id': rec.x_batch_id.id,
                    'x_start_date': rec.x_start_date,
                    'x_end_date': rec.x_end_date,
                    'x_date': line.x_date
                }
                rec.env['op.session'].create(session)
