# -*- coding: utf-8 -*-
{
    'name': "Management Timetable",

    'summary': """
        Management Timetable """,

    'description': """
        Management Timetable
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','manager_education'
    ],

    # always loaded
    'data': [
        # 'data/sequence.xml',
        'security/ir.model.access.csv',
        #views
        'views/op_sessions_view.xml',
        'views/op_timing_view.xml',

        #wizard
        'wizard/gen_timetable_view.xml',

        'views/menus.xml',
    ],
    'application': True,
}