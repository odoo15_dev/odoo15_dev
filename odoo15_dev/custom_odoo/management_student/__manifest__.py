# -*- coding: utf-8 -*-
{
    'name': "Management Student",

    'summary': """
        Management Student """,

    'description': """
        Management Student
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','manager_education',
    ],

    # always loaded
    'data': [
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/op_student_view.xml',
        'views/op_certificate.xml',
        'views/menus.xml',
    ],
    'application': True,
}