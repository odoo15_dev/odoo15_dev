from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpStudent(models.Model):
    _name = 'op.student'
    _rec_name = 'x_name'

    @api.model
    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'VN')], limit=1)
        return country

    x_name = fields.Char(string='Tên', required=True)
    x_code_student = fields.Char(string='Mã sinh viên', default=lambda self: _('New'), required=True)
    x_birth_date = fields.Date(string='Ngày sinh', required=True)
    x_user_id = fields.Many2one('res.users', string='Người dùng liên kết', required=True)
    x_id_card = fields.Char(string='CCCD/CMND', required=True)
    x_email = fields.Char(string='Email')
    x_national_id = fields.Many2one('res.country', string='Quốc tịch', default=_get_default_country,
                                    readonly=False, store=True)
    x_phone = fields.Char(string='Điện thoại')
    image_1920 = fields.Binary(string='Avatar')
    x_company_id = fields.Many2one('res.company', string='Công ty')
    x_gender = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('other', 'Khác')
    ], default='male', string='Giới tính')

    @api.model
    def create(self, vals):
        if vals.get('x_code_student', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.student.sequence')]).prefix
            padding = (self.env['ir.sequence'].next_by_code('op.student.sequence') or _('New...')).replace(prefix, '')
            vals['x_code_student'] = prefix + padding
        result = super(OpStudent, self).create(vals)
        return result

    @api.onchange('x_user_id')
    def _onchange_x_user_id(self):
        for rec in self:
            rec.x_company_id = rec.x_user_id.company_id.id
            if not rec.image_1920:
                rec.image_1920 = rec.x_user_id.image_1920
