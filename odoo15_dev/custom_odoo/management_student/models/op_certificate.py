from odoo import api, fields, models, _
from datetime import datetime
from datetime import timedelta
from odoo.exceptions import ValidationError, UserError
from dateutil.relativedelta import relativedelta


class OpCertificate(models.Model):
    _name = "op.certificate"
    _rec_name = "x_code"

    @api.model
    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'VN')], limit=1)
        return country

    x_student_id = fields.Many2one('op.student', string='Tên học viên', required=True)
    x_birth_date = fields.Date(string='Ngày sinh')
    x_id_card = fields.Char(string='CMND/CCCD')
    x_code = fields.Char(string='Mã chứng chỉ', default=lambda self: _('New'), required=True)
    x_date_create_certificate = fields.Date(string='Ngày tạo chứng chỉ', default=datetime.today())
    x_date_expiry = fields.Date(string='Ngày hết hạn chứng chỉ')
    x_certificate_attachment_ids = fields.Many2many('ir.attachment', 'op_certificate_attachment_rel',
                                                    'op_certificate_id',
                                                    'attachment_id', 'Attachments')
    x_type_allocation = fields.Selection([
        ('new_level', 'Cấp mới'),
        ('extend', 'Gia hạn')
    ], default='new_level', string='Loại cấp')
    x_note = fields.Text('Ghi chú')
    x_recommended_number = fields.Char('Số quyết định')
    x_recommended_date = fields.Date('Ngày quyết định')
    x_country_id = fields.Many2one('res.country', string='Quốc gia', default=_get_default_country,
                                   readonly=False, store=True)
    x_course_id = fields.Many2one('op.course', string='Khóa học')

    @api.model
    def create(self, vals):
        if vals.get('x_code', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.certificate.sequence')]).prefix
            padding = (self.env['ir.sequence'].next_by_code('op.certificate.sequence') or _('New...')).replace(prefix,
                                                                                                               '')
            vals['x_code'] = prefix + padding
        result = super(OpCertificate, self).create(vals)
        return result
