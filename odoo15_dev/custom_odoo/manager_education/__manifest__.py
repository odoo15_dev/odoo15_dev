# -*- coding: utf-8 -*-
{
    'name': "Management Education",

    'summary': """
        Management Education """,

    'description': """
        Management Education
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','management_faculty',
    ],

    # always loaded
    'data': [
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/op_course_view.xml',
        'views/op_subject_view.xml',
        'views/op_batch_view.xml',
        'views/menus.xml',
    ],
    'application': True,
}