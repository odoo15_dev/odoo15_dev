from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpSubject(models.Model):
    _name = 'op.subject'
    _rec_name = 'x_name'

    x_name = fields.Char(string='Tên môn', required=True)
    x_type_subject = fields.Selection([
        ('theory', 'Lý thuyết'),
        ('practical', 'Thực hành'),
        ('both', 'Lý thuyết & Thực hành')
    ], default='theory', string='Loại môn học')
    x_code_subject = fields.Char(string='Mã môn học', default=lambda self: _('New'), required=True)
    x_lesson_theory = fields.Integer(string='Số tiết lý thuyết')
    x_lesson_practical = fields.Integer(string='Số tiết thực hành')
    x_description = fields.Text(string='Mô tả')


    @api.model
    def create(self, vals):
        if vals.get('x_code_subject', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.subject.sequence')]).prefix
            padding = (self.env['ir.sequence'].next_by_code('op.subject.sequence') or _('New...')).replace(prefix, '')
            vals['x_code_subject'] = prefix + padding
        result = super(OpSubject, self).create(vals)
        return result

