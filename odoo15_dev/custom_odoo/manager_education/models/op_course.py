from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpCourse(models.Model):
    _name = 'op.course'
    _rec_name = 'x_name'

    x_name = fields.Char(string='Tên khóa học', required=True)
    x_code_course = fields.Char(string='Mã khóa học', default=lambda self: _('New'), required=True)
    x_type_certificate = fields.Selection([
        ('limited', 'Có giới hạn'),
        ('unlimited', 'Không giới hạn')
    ], default='limited', string='Loại chứng chỉ')
    x_min_student = fields.Integer(string='Học viên tối thểu')
    x_max_student = fields.Integer(string='Học viên tối đa')
    x_time_limit = fields.Integer(string='Thời hạn chứng chỉ')
    x_course_line_ids = fields.One2many('op.course.line', 'x_course_id', string='Môn học')
    x_description = fields.Text(string='Mô tả')

    @api.model
    def create(self, vals):
        if vals.get('x_code_course', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.course.sequence')]).prefix
            padding = (self.env['ir.sequence'].next_by_code('op.course.sequence') or _('New...')).replace(prefix, '')
            vals['x_code_course'] = prefix + padding
        result = super(OpCourse, self).create(vals)
        return result


class OpCourseLine(models.Model):
    _name = 'op.course.line'

    x_course_id = fields.Many2one('op.course',string='Khóa học')
    x_batch_id = fields.Many2one('op.batch', string='Lịch đào học')

    x_subject_id = fields.Many2one('op.subject',string='Môn học')
    x_type_subject = fields.Selection([
        ('theory', 'Lý thuyết'),
        ('practical', 'Thực hành'),
        ('both', 'Lý thuyết & Thực hành')
    ], default='theory', string='Loại môn học')
    x_code_subject = fields.Char(string='Mã môn học', default=lambda self: _('New'), required=True)
    x_lesson_theory = fields.Integer(string='Số tiết lý thuyết')
    x_lesson_practical = fields.Integer(string='Số tiết thực hành')

    @api.onchange('x_subject_id')
    def _onchange_x_subject_id(self):
        for rec in self:
            rec.write({
                'x_type_subject': rec.x_subject_id.x_type_subject,
                'x_code_subject': rec.x_subject_id.x_code_subject,
                'x_lesson_theory': rec.x_subject_id.x_lesson_theory,
                'x_lesson_practical': rec.x_subject_id.x_lesson_practical,
            })
