from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpBatch(models.Model):
    _name = 'op.batch'
    _rec_name = 'x_code_batch'

    x_code_batch = fields.Char(string='Mã lịch đào tạo', default=lambda self: _('New'), required=True)
    x_start_date = fields.Date(string='Ngày bắt đầu')
    x_end_date = fields.Date(string='Ngày kết thúc')
    x_course_id = fields.Many2one('op.course', string='Khóa học')
    x_faculty_id = fields.Many2one('op.faculty', string='Giảng viên chủ nhiệm')
    x_faculty_ids = fields.Many2many('op.faculty', 'rel_x_faculty_ids', string='Giảng viên liên quan')
    x_location = fields.Char(string='Địa điểm học')
    state = fields.Selection([('draft', 'Nháp'),
                              ('prepare', 'Chuẩn bị'),
                              ('studying', 'Đang học'),
                              ('complete', 'Hoàn Thành'),
                              ('certificate', 'Đã cấp chứng chỉ'),
                              ('cancel', 'Hủy')], required=True, default='draft', tracking=True)

    x_course_line_ids = fields.One2many('op.course.line', 'x_batch_id', string='Môn học')

    @api.model
    def create(self, vals):
        if vals.get('x_code_batch', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.batch.sequence')]).prefix
            course = self.env['op.course'].browse(vals.get('x_course_id'))
            padding = (self.env['ir.sequence'].next_by_code('op.batch.sequence') or _('New...')).replace(prefix, '')
            vals['x_code_batch'] = prefix + course.x_code_course + padding
        result = super(OpBatch, self).create(vals)
        return result

    def button_prepare(self):
        for rec in self:
            rec.write({
                'state': 'prepare'
            })

    def button_studying(self):
        for rec in self:
            rec.write({
                'state': 'studying'
            })

    def button_complete(self):
        for rec in self:
            rec.write({
                'state': 'complete'
            })

    def button_certificate(self):
        for rec in self:
            rec.write({
                'state': 'certificate'
            })

    def button_cancel(self):
        for rec in self:
            rec.write({
                'state': 'cancel'
            })
