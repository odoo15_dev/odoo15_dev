# -*- coding: utf-8 -*-
{
    'name': "Management Booking Course",

    'summary': """
        Management Booking Register """,

    'description': """
        Management Booking Register
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','manager_admission','manager_education',
    ],

    # always loaded
    'data': [
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/op_booking_view.xml',
        'views/op_register_view.xml',
        'views/op_batch_view.xml',

        'views/menus.xml',
    ],
    'application': True,
}