from odoo import models, fields, api, _
from odoo.http import request
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpAdmission(models.Model):
    _inherit = 'op.admission'

    x_booking_id = fields.Many2one('op.booking',string='Booking khóa học')
    x_register_id = fields.Many2one('op.register', string='Đăng ký khóa học')
    x_batch_id = fields.Many2one('op.batch',string='Lịch đào tạo')

