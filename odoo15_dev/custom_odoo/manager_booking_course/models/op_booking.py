from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpBooking(models.Model):
    _name = 'op.booking'
    _rec_name = 'x_name_booking'

    x_name_booking = fields.Char(string='Tên khóa booking', default=lambda self: _('New'), required=True)
    x_course_id = fields.Many2one('op.course', string='Khóa học')
    x_start_date = fields.Date(string='Ngày bắt đầu')
    x_end_date = fields.Date(string='Ngày kết thúc')
    x_fee = fields.Float(string='Học phí')
    x_location = fields.Char(string='Địa điểm học')
    x_partner_id = fields.Many2one('res.partner', string='Người liên hệ')
    x_email = fields.Char(string='Email liên hệ')
    x_phone = fields.Char(string='Số điện thoại liên hệ')
    x_op_admission_line_ids = fields.One2many('op.admission', 'x_booking_id', string='Hồ sơ sinh viên')
    x_note = fields.Char(string='Ghi chú')
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('confirm', 'Xác nhận'),
        ('cancel', 'Hủy')
    ], default='draft', string='Trạng thái')

    @api.model
    def create(self, vals):
        if vals.get('x_name_booking', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.booking.sequence')]).prefix
            course = self.env['op.course'].browse(vals.get('x_course_id'))
            padding = (self.env['ir.sequence'].next_by_code('op.booking.sequence') or _('New...')).replace(prefix, '')
            vals['x_name_booking'] = prefix + '/' + course.x_code_course + padding
        result = super(OpBooking, self).create(vals)
        return result

    @api.onchange('x_partner_id')
    def _onchange_x_partner_id(self):
        for rec in self:
            rec.x_email = rec.x_partner_id.email
            rec.x_phone = rec.x_partner_id.phone

    def button_confirm(self):
        for rec in self:
            rec.write({
                'state': 'confirm'
            })
            op_register = {
                'x_course_id': rec.x_course_id.id,
                'x_start_date': rec.x_start_date,
                'x_end_date': rec.x_end_date,
                'x_fee': rec.x_fee,
                'x_min_student': rec.x_course_id.x_min_student,
                'x_max_student': rec.x_course_id.x_max_student,
                'x_booking_id': rec.id
            }
            op_register_obj = rec.env['op.register'].create(op_register)
            if rec.x_op_admission_line_ids:
                for line in rec.x_op_admission_line_ids:
                    line.x_register_id = op_register_obj.id
                    line.x_status = 'register'

    def button_cancel(self):
        for rec in self:
            rec.write({
                'state': 'cancel'
            })
