from odoo import models, fields, api, _
from odoo.http import request
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpBatch(models.Model):
    _inherit = 'op.batch'

    x_register_id = fields.Many2one('op.register', string='Đợt đăng ký')
    x_op_admission_line_ids = fields.One2many('op.admission', 'x_batch_id', string='Hồ sơ sinh viên')
