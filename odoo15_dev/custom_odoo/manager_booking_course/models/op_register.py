from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpRegister(models.Model):
    _name = 'op.register'
    _rec_name = 'x_name_register'

    x_name_register = fields.Char(string='Hồ sơ đăng ký', default=lambda self: _('New'), required=True)
    x_course_id = fields.Many2one('op.course', string='Khóa học')
    x_start_date = fields.Date(string='Ngày bắt đầu')
    x_end_date = fields.Date(string='Ngày kết thúc')
    x_fee = fields.Float(string='Học phí')
    x_min_student = fields.Integer(string='Học viên tối thểu')
    x_max_student = fields.Integer(string='Học viên tối đa')
    x_op_admission_line_ids = fields.One2many('op.admission', 'x_register_id', string='Hồ sơ sinh viên')
    x_booking_id = fields.Many2one('op.booking', string='Booking khóa học')
    state = fields.Selection([
        ('draft', 'Nháp'),
        ('confirm', 'Xác nhận'),
        ('done', 'Hoàn thành'),
        ('cancel', 'Hủy')
    ], default='draft', string='Trạng thái')

    @api.model
    def create(self, vals):
        if vals.get('x_name_register', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.register.sequence')]).prefix
            course = self.env['op.course'].browse(vals.get('x_course_id'))
            padding = (self.env['ir.sequence'].next_by_code('op.register.sequence') or _('New...')).replace(prefix, '')
            vals['x_name_register'] = prefix + '/' + course.x_code_course + padding
        result = super(OpRegister, self).create(vals)
        return result

    def button_confirm(self):
        for rec in self:
            rec.write({
                'state': 'confirm'
            })
            for line in rec.x_op_admission_line_ids:
                line.x_status = 'studying'

    def button_cancel(self):
        for rec in self:
            rec.write({
                'state': 'cancel'
            })

    def button_done(self):
        for rec in self:
            rec.write({
                'state': 'done'
            })
            batch = {
                'x_start_date': rec.x_start_date,
                'x_end_date': rec.x_end_date,
                'x_course_id': rec.x_course_id.id,
                'x_location': rec.x_booking_id.x_location,
                'x_register_id': rec.id
            }
            batch_obj = rec.env['op.batch'].create(batch)
            if rec.x_course_id.x_course_line_ids:
                for line_course in rec.x_course_id.x_course_line_ids:
                    line_course.x_batch_id = batch_obj.id
            if rec.x_op_admission_line_ids:
                for line_admission in rec.x_op_admission_line_ids:
                    line_admission.x_batch_id = batch_obj.id
                    line_admission.x_status = 'studied'
