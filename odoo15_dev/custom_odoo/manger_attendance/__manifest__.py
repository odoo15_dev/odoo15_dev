# -*- coding: utf-8 -*-
{
    'name': "Management Attendance",

    'summary': """
        Management Attendance """,

    'description': """
        Management Attendance
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','manager_timetable',
    ],

    # always loaded
    'data': [
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/op_attendance_view.xml',


        'views/menus.xml',
    ],
    'application': True,
}