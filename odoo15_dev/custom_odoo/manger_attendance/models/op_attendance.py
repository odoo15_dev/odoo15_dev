from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpAttendance(models.Model):
    _name = 'op.attendance'
    _rec_name = 'x_code_attendance'

    x_code_attendance = fields.Char(string='Mã điểm danh', default=lambda self: _('New'), required=True)
    x_session_id = fields.Many2one('op.session', string='Lịch giảng', required=True)
    x_date = fields.Date(string='Ngày')
    x_start_date = fields.Date(string='Ngày bắt đầu')
    x_end_date = fields.Date(string='Ngày kết thúc')
    x_batch_id = fields.Many2one('op.batch', string='Lịch đào tạo', compute='_compute_x_batch_id', store=True)
    x_attendance_line_ids = fields.One2many('op.attendance.line', 'x_attendance_id', string='Thông tin điểm danh')

    @api.onchange('x_session_id')
    def _onchange_x_session_id(self):
        for rec in self:
            rec.write({
                'x_start_date': rec.x_session_id.x_start_date,
                'x_end_date': rec.x_session_id.x_end_date,
                'x_date': rec.x_session_id.x_date,
            })

    @api.depends('x_session_id')
    def _compute_x_batch_id(self):
        for rec in self:
            if rec.x_session_id:
                rec.x_batch_id = rec.x_session_id.x_batch_id.id
            else:
                rec.x_batch_id = None

    @api.onchange('x_session_id')
    def _onchange_x_attendance_line_ids(self):
        for rec in self:
            if rec.x_attendance_line_ids:
                rec.x_attendance_line_ids.unlink()
            if rec.x_session_id:
                vals = []
                if rec.x_session_id.x_batch_id.x_op_admission_line_ids:
                    for line in rec.x_session_id.x_batch_id.x_op_admission_line_ids:
                        vals_line = {
                            'x_student_id': line.x_student_id.id,
                        }
                        vals.append((0, 0, vals_line))
                if vals:
                    rec.write({
                        'x_attendance_line_ids': vals,
                    })
                else:
                    rec.write({
                        'x_attendance_line_ids': None,
                    })

    @api.model
    def create(self, vals):
        if vals.get('x_code_attendance', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.course.sequence')]).prefix
            padding = (self.env['ir.sequence'].next_by_code('op.course.sequence') or _('New...')).replace(prefix, '')
            vals['x_code_attendance'] = prefix + padding
        result = super(OpAttendance, self).create(vals)
        return result


class OpAttendanceLine(models.Model):
    _name = 'op.attendance.line'

    x_student_id = fields.Many2one('op.student', string='Sinh viên', required=True)
    x_present = fields.Boolean(
        'Hiện diện', default=False, tracking=True)
    x_attendance_id = fields.Many2one('op.attendance', string='Điểm danh')
    x_absent = fields.Boolean('Vắng mặt', default=True, tracking=True)
    x_note = fields.Char(string='Nhận xét')

    @api.onchange('x_present', 'x_absent')
    def _onchange_x_present_x_absent(self):
        for rec in self:
            if rec.x_present:
                rec.x_absent = False
            if rec.x_absent:
                rec.x_present = False
