# -*- coding: utf-8 -*-
{
    'name': "Management Admission",

    'summary': """
        Management Admission """,

    'description': """
        Management Admission
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base','management_student',
    ],

    # always loaded
    'data': [
        'data/sequence.xml',
        'security/ir.model.access.csv',
        'views/op_admission_view.xml',

        # 'views/menus.xml',
    ],
    'application': True,
}