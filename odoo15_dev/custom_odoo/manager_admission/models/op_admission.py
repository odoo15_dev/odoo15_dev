from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class OpAdmission(models.Model):
    _name = 'op.admission'
    _rec_name = 'x_name_file'

    @api.model
    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'VN')], limit=1)
        return country

    x_name_file = fields.Char(string='Mã hồ sơ sinh viên', default=lambda self: _('New'), required=True)
    x_student_id = fields.Many2one('op.student', string='Sinh viên', required=True)
    x_birth_date = fields.Date(string='Ngày sinh', required=True)
    x_id_card = fields.Char(string='CCCD/CMND', required=True)
    x_email = fields.Char(string='Email')
    x_national_id = fields.Many2one('res.country', string='Quốc tịch', default=_get_default_country,
                                    readonly=False, store=True)
    x_phone = fields.Char(string='Điện thoại')
    x_gender = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('other', 'Khác')
    ], default='male', string='Giới tính')
    x_type_participation = fields.Selection([
        ('offline', 'Trực tiếp'),
        ('online', 'Trực tuyến')
    ], default='offline', string='Hình thức học')
    x_status = fields.Selection(
        [('draft', 'Nháp'),
         ('register', 'Đang đăng ký học'),
         ('studying', 'Đang nhập học'),
         ('studied', 'Đã nhập học'),
         ('complete', 'Hoàn thành'),
         ('not_complete', 'Không hoàn thành'),
         ('not_studying', 'Hủy nhập học')],
        'Trạng thái', default='draft', )

    @api.model
    def create(self, vals):
        if vals.get('x_name_file', _('New')) == _('New'):
            prefix = self.env['ir.sequence'].sudo().search([('code', '=', 'op.admission.sequence')]).prefix
            student = self.env['op.student'].browse(vals.get('x_student_id'))
            padding = (self.env['ir.sequence'].next_by_code('op.admission.sequence') or _('New...')).replace(prefix, '')
            vals['x_name_file'] = prefix +'/'+ student.x_code_student + padding
        result = super(OpAdmission, self).create(vals)
        return result

    @api.onchange('x_student_id')
    def _onchange_x_student_id(self):
        for rec in self:
            rec.write({
                'x_birth_date': rec.x_student_id.x_birth_date,
                'x_id_card': rec.x_student_id.x_id_card,
                'x_email': rec.x_student_id.x_email,
                'x_national_id': rec.x_student_id.x_national_id.id,
                'x_phone': rec.x_student_id.x_phone,
                'x_gender': rec.x_student_id.x_gender
            })
