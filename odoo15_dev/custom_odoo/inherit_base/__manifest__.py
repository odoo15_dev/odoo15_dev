# -*- coding: utf-8 -*-
{
    'name': "Inherit Base",

    'summary': """
        Inherit Base """,

    'description': """
        Inherit Base
    """,

    'author': "Quy",
    'website': "https://www.enmasys.com",

    'category': '',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'base',
    ],

    # always loaded
    'data': [


    ],
    'installable': True,
    'application': True,
}